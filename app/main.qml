/*
 * Copyright (C) 2018 The Qt Company Ltd.
 * Copyright (c) 2018-2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import QtQuick 2.6
import QtQuick.Controls 2.0
import AGL.Demo.Controls 1.0
import NativeAppModel 1.0
import ServerAppModel 1.0

import 'pages'

ApplicationWindow {
    id: root

    width: 1080 * screenInfo.scale_factor()
    height: 1487 * screenInfo.scale_factor()

    SwipeView {
        id: stackLayout
        anchors.fill: parent
        currentIndex: tabBar.currentIndex

        ListPage {
            id: listPage

            model: ServerAppModel {
                id: listModel
            }
        }
        SearchList {
            id: searchPage

            model:ServerAppModel {
                id: searchModel
            }
        }

        ManagementPage {
            id: managementPage

            model: NativeAppModel {
                id: managementModel
            }
        }

        Connections {
            target: managementPage.model
            onApplistChanged: {
                listPage.model.setNativeApplist(applist)
                searchPage.model.setNativeApplist(applist)
            }
        }

        Component.onCompleted: {
            listPage.model.getPrevPage(0)
            managementPage.model.refresh()
        }
    }


    footer: TabBar {
        id: tabBar
        height: 80
        currentIndex: stackLayout.currentIndex

        TabButton {
            text: "List"
            font.pixelSize: 20
            height: parent.height
        }
        TabButton {
            text: "Search"
            font.pixelSize: 20
            height: parent.height
        }
        TabButton {
            text: "Installed Application"
            font.pixelSize: 20
            height: parent.height
        }
    }
}
