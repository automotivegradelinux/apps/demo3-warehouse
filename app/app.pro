TEMPLATE = app
TARGET = warehouse
QT += quickcontrols2 dbus websockets

DESTDIR = $${OUT_PWD}/../package/root/bin

CONFIG += c++11 link_pkgconfig
PKGCONFIG += qlibhomescreen qlibwindowmanager

include(../interfaces/interfaces.pri)
include(src/src.pri)

OTHER_FILES += \
    main.qml

RESOURCES += \
    resources.qrc \
    images/images.qrc
