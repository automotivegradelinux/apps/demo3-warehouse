/*
 * Copyright (C) 2016 The Qt Company Ltd.
 * Copyright (C) 2016, 2017 Mentor Graphics Development (Deutschland) GmbH
 * Copyright (c) 2018-2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nativeappmodel.h"
#include <QtDBus/QDBusInterface>
#include <QtDBus/QDBusReply>
#include "afm_user_daemon_proxy.h"
#include "httpclient.h"

#include "hmi-debug.h"

extern org::AGL::afm::user* afm_user_daemon_proxy;

class NativeAppModel::Private {
 public:
  Private();

  void getApps();

  QList<AppInfo> data;
};

NativeAppModel::Private::Private() {
  // this->getApps();
}

void NativeAppModel::Private::getApps() {
  QString apps = afm_user_daemon_proxy->runnables(QStringLiteral(""));
  QJsonDocument japps = QJsonDocument::fromJson(apps.toUtf8());
  for (auto const& app : japps.array()) {
    QJsonObject const& jso = app.toObject();

    AppInfo appinfo;
    appinfo.read(jso);

    this->data.append(appinfo);
  }
}

NativeAppModel::NativeAppModel(QObject* parent)
    : QAbstractListModel(parent), d(new Private()) {
  connect(afm_user_daemon_proxy, &org::AGL::afm::user::changed, this,
          &NativeAppModel::appChanged);
}

NativeAppModel::~NativeAppModel() {
  delete this->d;
}

int NativeAppModel::rowCount(const QModelIndex& parent) const {
  if (parent.isValid())
    return 0;

  return this->d->data.count();
}

QVariant NativeAppModel::data(const QModelIndex& index, int role) const {
  QVariant ret;
  if (!index.isValid())
    return ret;

  switch (role) {
    case IconRole:
      ret = this->d->data[index.row()].iconPath();
      break;
    case NameRole:
      ret = this->d->data[index.row()].name();
      break;
    case IdRole:
      ret = this->d->data[index.row()].id();
      break;
    case VersionRole:
      ret = this->d->data[index.row()].version();
      break;
    case DescriptionRole:
      ret = this->d->data[index.row()].description();
      break;
    case ShortNameRole:
      ret = this->d->data[index.row()].shortname();
      break;
    case AuthorRole:
      ret = this->d->data[index.row()].author();
      break;
    default:
      break;
  }

  return ret;
}

QHash<int, QByteArray> NativeAppModel::roleNames() const {
  QHash<int, QByteArray> roles;
  roles[IconRole] = "icon";
  roles[NameRole] = "name";
  roles[IdRole] = "id";
  roles[VersionRole] = "version";
  roles[DescriptionRole] = "description";
  roles[ShortNameRole] = "shortname";
  roles[AuthorRole] = "author";
  return roles;
}

QString NativeAppModel::id(int i) const {
  return data(index(i), IdRole).toString();
}

QString NativeAppModel::name(int i) const {
  return data(index(i), NameRole).toString();
}

void NativeAppModel::appChanged(const QString& info) {
  this->refresh();
}

int NativeAppModel::launch(const QString& application) {
  int result = -1;
  HMI_DEBUG("launch", "ApplicationLauncher launch %s.",
            application.toStdString().c_str());

  result = afm_user_daemon_proxy->start(application).value().toInt();
  HMI_DEBUG("launch", "ApplicationLauncher pid: %d.", result);

  return result;
}

void NativeAppModel::uninstall(int index) {
  const QString& id = this->d->data[index].id();
  QStringList list = id.split("@");
  QString& name = list[0];

  //afm_user_daemon_proxy->terminate(id);

  QString result = afm_user_daemon_proxy->uninstall(id);

  if (result == "null") {
      QString pathName = "~/app-data/" + name + "/";

      if(!name.isEmpty()){
        QString cmd = "rm -r " + pathName;
        qDebug() <<"CMD:"<< cmd;
        system(cmd.toStdString().c_str());
      }

      pathName = "/var/local/lib/afm/applications/" + name + "/";
      QFileInfo file(pathName);
      if(file.isDir() && !name.isEmpty())
      {
        QString cmd = "rm -r " + pathName;
        qDebug() <<"CMD:"<< cmd;
        system(cmd.toStdString().c_str());
      }

    beginRemoveRows(QModelIndex(), index, index);
    this->d->data.removeAt(index);
    endRemoveRows();
  }
}

void NativeAppModel::refresh() {
  beginResetModel();
  this->d->data.clear();
  this->d->getApps();
  endResetModel();
  emit applistChanged(this->d->data);
}
