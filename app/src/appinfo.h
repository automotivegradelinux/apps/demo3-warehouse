/*
 * Copyright (C) 2016, 2017 Mentor Graphics Development (Deutschland) GmbH
 * Copyright (C) 2016 The Qt Company Ltd.
 * Copyright (c) 2018-2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef APPINFO_H
#define APPINFO_H

#include <QtCore/QJsonObject>
#include <QtCore/QObject>
#include <QtCore/QSharedDataPointer>

class AppInfo {
  Q_GADGET
  Q_PROPERTY(QString id READ id)
  Q_PROPERTY(QString version READ version)
  Q_PROPERTY(int width READ width)
  Q_PROPERTY(int height READ height)
  Q_PROPERTY(QString name READ name)
  Q_PROPERTY(QString description READ description)
  Q_PROPERTY(QString shortname READ shortname)
  Q_PROPERTY(QString author READ author)
  Q_PROPERTY(QString iconPath READ iconPath)
  Q_PROPERTY(AppState state READ state WRITE setState)
  Q_PROPERTY(qreal progress READ progress WRITE setProgress)
  Q_PROPERTY(QString serverid READ serverId)
  Q_PROPERTY(QString wgtpath READ wgtPath)
  Q_PROPERTY(QString filename READ fileName)
  Q_PROPERTY(QString categoryid READ categoryId)
  Q_PROPERTY(QString categoryname READ categoryName)
  Q_PROPERTY(QString deviceid READ deviceId)
  Q_PROPERTY(QString devicename READ deviceName)
  Q_PROPERTY(double createdtime READ createdTime)
 public:
  enum AppState { Install = 0, Update, Launch, Downloading, Installing };
  Q_ENUM(AppState)

  AppInfo();
  AppInfo(const QString& icon, const QString& name, const QString& id);
  AppInfo(const AppInfo& other);
  virtual ~AppInfo();
  AppInfo& operator=(const AppInfo& other);
  bool operator==(const AppInfo& other);
  void swap(AppInfo& other) { qSwap(d, other.d); }

  QString id() const;
  QString version() const;
  int width() const;
  int height() const;
  QString name() const;
  QString description() const;
  QString shortname() const;
  QString author() const;
  QString iconPath() const;
  AppState state() const;
  qreal progress() const;

  QString serverId() const;
  QString wgtPath() const;
  QString fileName() const;
  QString categoryId() const;
  QString categoryName() const;
  QString deviceId() const;
  QString deviceName() const;
  double createdTime() const;

  void setState(const AppState state);
  void setState(const int state);
  void setProgress(const qreal progress);

  void read(const QJsonObject& json);
  void readFromServer(const QJsonObject& json);

 private:
  class Private;
  QSharedDataPointer<Private> d;
};

Q_DECLARE_SHARED(AppInfo)
Q_DECLARE_METATYPE(AppInfo)
Q_DECLARE_METATYPE(QList<AppInfo>)

#endif  // APPINFO_H
