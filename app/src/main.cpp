/*
 * Copyright (C) 2018 The Qt Company Ltd.
 * Copyright (c) 2018-2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <QtQml/qqml.h>
#include <qlibwindowmanager.h>
#include <QQuickWindow>
#include <QtCore/QCommandLineParser>
#include <QtCore/QDebug>
#include <QtCore/QDir>
#include <QtCore/QStandardPaths>
#include <QtCore/QUrlQuery>
#include <QtGui/QGuiApplication>
#include <QtQml/QQmlApplicationEngine>
#include <QtQml/QQmlContext>
#include <QtQuickControls2/QQuickStyle>
#include "afm_user_daemon_proxy.h"
#include "nativeappmodel.h"
#include "qlibhomescreen.h"
#include "serverappmodel.h"

org::AGL::afm::user* afm_user_daemon_proxy;

namespace {

struct Cleanup {
  static inline void cleanup(org::AGL::afm::user* p) {
    delete p;
    afm_user_daemon_proxy = Q_NULLPTR;
  }
};

void noOutput(QtMsgType, const QMessageLogContext&, const QString&) {}

}  // namespace

int main(int argc, char* argv[]) {
  QString role = QString("warehouse");
  QGuiApplication app(argc, argv);

  // use launch process
  QScopedPointer<org::AGL::afm::user, Cleanup> afm_user_daemon_proxy(
      new org::AGL::afm::user("org.AGL.afm.user", "/org/AGL/afm/user",
                              QDBusConnection::sessionBus(), 0));
  ::afm_user_daemon_proxy = afm_user_daemon_proxy.data();

  app.setApplicationName("warehouse");

  QQuickStyle::setStyle("AGL");

  QQmlApplicationEngine engine;
  QQmlContext* context = engine.rootContext();

  QCommandLineParser parser;
  parser.addPositionalArgument("port",
                               app.translate("main", "port for binding"));
  parser.addPositionalArgument("secret",
                               app.translate("main", "secret for binding"));
  parser.addHelpOption();
  parser.addVersionOption();
  parser.process(app);
  QStringList positionalArguments = parser.positionalArguments();

  if (positionalArguments.length() == 2) {
    int port = positionalArguments.takeFirst().toInt();
    QString secret = positionalArguments.takeFirst();
    QUrl bindingAddress;
    bindingAddress.setScheme(QStringLiteral("ws"));
    bindingAddress.setHost(QStringLiteral("localhost"));
    bindingAddress.setPort(port);
    bindingAddress.setPath(QStringLiteral("/api"));
    QUrlQuery query;
    query.addQueryItem(QStringLiteral("token"), secret);
    bindingAddress.setQuery(query);

    std::string token = secret.toStdString();

    // import C++ class to QML
    qmlRegisterType<NativeAppModel>("NativeAppModel", 1, 0, "NativeAppModel");
    qmlRegisterType<ServerAppModel>("ServerAppModel", 1, 0, "ServerAppModel");

    QLibHomeScreen* homescreenHandler = new QLibHomeScreen();
    QLibWindowmanager* qwm = new QLibWindowmanager();

    // WindowManager
    if (qwm->init(port, secret) != 0) {
      exit(EXIT_FAILURE);
    }

    AGLScreenInfo screenInfo(qwm->get_scale_factor());

    // Request a surface as described in layers.json windowmanager’s file
    if (qwm->requestSurface(role) != 0) {
      exit(EXIT_FAILURE);
    }

    // Create an event callback against an event type. Here a lambda is
    // called when SyncDraw event occurs
    qwm->set_event_handler(QLibWindowmanager::Event_SyncDraw,
                           [qwm, role](json_object* object) {
                             fprintf(stderr, "Surface got syncDraw!\n");

                             qwm->endDraw(role);
                           });

    // HomeScreen
    homescreenHandler->init(port, token.c_str());
    // Set the event handler for Event_TapShortcut which will activate the
    // surface for windowmanager
    homescreenHandler->set_event_handler(
        QLibHomeScreen::Event_TapShortcut, [qwm, role](json_object* object) {
            qDebug("Surface warehouse got tapShortcut.\n");
            struct json_object *obj_param = nullptr, *obj_area = nullptr;
            if(json_object_object_get_ex(object, "parameter", &obj_param)
            && json_object_object_get_ex(obj_param, "area", &obj_area)) {
                qwm->activateWindow(role.toStdString().c_str(), json_object_get_string(obj_area));
            }
            else {
                qwm->activateWindow(role.toStdString().c_str(), "normal");
            }
        });

    context->setContextProperty(QStringLiteral("homescreenHandler"),
                                homescreenHandler);
    context->setContextProperty(QStringLiteral("screenInfo"), &screenInfo);
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    QObject* root = engine.rootObjects().first();

    QQuickWindow* window = qobject_cast<QQuickWindow*>(root);
    QObject::connect(window, SIGNAL(frameSwapped()), qwm,
                     SLOT(slotActivateSurface()));
  }
  return app.exec();
}
