
INCLUDEPATH += $$PWD

HEADERS += \
    $$PWD/nativeappmodel.h \
    $$PWD/serverappmodel.h \
    $$PWD/appinfo.h \
    $$PWD/hmi-debug.h \
    $$PWD/httpclient.h \
    $$PWD/config.h

SOURCES += \
    $$PWD/main.cpp \
    $$PWD/nativeappmodel.cpp \
    $$PWD/serverappmodel.cpp \
    $$PWD/appinfo.cpp \
    $$PWD/httpclient.cpp
