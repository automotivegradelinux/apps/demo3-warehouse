/*
 * Copyright (C) 2016 The Qt Company Ltd.
 * Copyright (c) 2018-2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SERVERAPPMODEL_H
#define SERVERAPPMODEL_H

#include <QtCore/QAbstractListModel>
#include "appinfo.h"
#include "config.h"

class ServerAppModel : public QAbstractListModel {
  Q_OBJECT

 public:
  enum ModelRole {
    IconRole = Qt::DisplayRole,
    NameRole = Qt::DecorationRole,
    IdRole = Qt::UserRole,
    VersionRole,
    DescriptionRole,
    AuthorRole,
    ServerIdRole,
    CategoryNameRole,
    CreatedTimeRole,
    StateRole,
    StateTextRole,
    ProgressRole
  };
  Q_ENUM(ModelRole)

  explicit ServerAppModel(QObject* parent = nullptr);
  ~ServerAppModel();

  int rowCount(const QModelIndex& parent = QModelIndex()) const override;

  QVariant data(const QModelIndex& index,
                int role = Qt::DisplayRole) const override;
  QHash<int, QByteArray> roleNames() const override;
  bool setData(const QModelIndex& index,
               const QVariant& value,
               int role) override;
  bool removeRows(int row,
                  int count,
                  const QModelIndex& parent = QModelIndex()) override;
  Q_INVOKABLE QString id(int index) const;
  Q_INVOKABLE QString name(int index) const;
  Q_INVOKABLE QString stateText(int index) const;
  Q_INVOKABLE void install(int index);
  Q_INVOKABLE int launch(const QString& application);

  Q_INVOKABLE void clearList();
  Q_INVOKABLE void getPrevPage(int PageIndex, QString name = "", int type = -1);
  Q_INVOKABLE void getNextPage(int PageIndex, QString name = "", int type = -1);

  Q_INVOKABLE void setNativeApplist(const QList<AppInfo>& applist);

  void getAppPage(int pageIndex,
                  int pageSize,
                  QString name,
                  int type,
                  bool isPrev = false);

  void appChanged(const QString& info);

 signals:
  void requestCompleted(int offsetSize,
                        int pageIndex,
                        int pageSize,
                        bool isPrev);

 private:
  void checkAppState();

  QList<AppInfo> applist;
  QList<AppInfo> nativeApplist;
};

#endif  // SERVERAPPMODEL_H
