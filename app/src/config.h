/*
 * Copyright (C) 2016 The Qt Company Ltd.
 * Copyright (c) 2018-2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef CONFIG_H
#define CONFIG_H

// server url config
#define SERVER_DOMAIN "warehouse.tmc-tokai.jp"

#define SERVER_DOMAIN_SERVICE QString("%1/webservice").arg(SERVER_DOMAIN)

#define SERVER_BASE_URL "/api/v1/app"
#define SERVER_API_LIST "/collection"

#define getURL(api) \
  QString("http://%1%2%3").arg(SERVER_DOMAIN_SERVICE, SERVER_BASE_URL, api)
#define getUrlWithPage(api, offset, limit)                      \
  getURL(api).append(                                           \
      QString("?sort=createDate&order=desc&offset=%1&limit=%2") \
          .arg(QString::number(offset), QString::number(limit)))

//#define getWgtUrl(path, typeId, appId) \
//  QString("http://%1%2/file/%3/%4/%5") \
//     .arg(SERVER_DOMAIN_SERVICE, SERVER_BASE_URL, path, typeId, appId)
#define getWgtUrl(path)                   \
  QString("http://%1%2/file?filePath=%3") \
      .arg(SERVER_DOMAIN_SERVICE, SERVER_BASE_URL, path)

#define getIconUrl(path) QString("http://%1%2").arg(SERVER_DOMAIN, path)

// server app page config
#define PAGE_SIZE 20

#define getDownloadFilePath(filename) QString("/tmp/%1").arg(filename)

#endif  // !CONFIG_H
