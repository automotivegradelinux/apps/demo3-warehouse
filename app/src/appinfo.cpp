/*
 * Copyright (C) 2016, 2017 Mentor Graphics Development (Deutschland) GmbH
 * Copyright (C) 2016 The Qt Company Ltd.
 * Copyright (c) 2018-2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "appinfo.h"
#include <QFileInfo>
#include "config.h"
#include "hmi-debug.h"

class AppInfo::Private : public QSharedData {
 public:
  Private();
  Private(const Private& other);

  QString id;
  QString version;
  int width;
  int height;
  QString name;
  QString description;
  QString shortname;
  QString author;
  QString iconPath;
  AppState state;
  qreal progress;

  QString serverid;
  QString wgtpath;
  QString filename;
  QString categoryid;
  QString categoryname;
  QString deviceid;
  QString devicename;
  double createdtime;
};

AppInfo::Private::Private() : width(-1), height(-1) {}

AppInfo::Private::Private(const Private& other)
    : QSharedData(other),
      id(other.id),
      version(other.version),
      width(other.width),
      height(other.height),
      name(other.name),
      description(other.description),
      shortname(other.shortname),
      author(other.author),
      iconPath(other.iconPath),
      state(other.state),
      progress(other.progress),
      serverid(other.serverid),
      wgtpath(other.wgtpath),
      filename(other.filename),
      categoryid(other.categoryid),
      categoryname(other.categoryname),
      deviceid(other.deviceid),
      devicename(other.devicename),
      createdtime(other.createdtime) {}

AppInfo::AppInfo() : d(new Private) {}

AppInfo::AppInfo(const QString& icon, const QString& name, const QString& id)
    : d(new Private) {
  d->iconPath = icon;
  d->name = name;
  d->id = id;
}

AppInfo::AppInfo(const AppInfo& other) : d(other.d) {}

AppInfo::~AppInfo() {}

AppInfo& AppInfo::operator=(const AppInfo& other) {
  d = other.d;
  return *this;
}

bool AppInfo::operator==(const AppInfo& other) {
  return d->id == other.id();
}

QString AppInfo::id() const {
  return d->id;
}

QString AppInfo::version() const {
  return d->version;
}

int AppInfo::width() const {
  return d->width;
}

int AppInfo::height() const {
  return d->height;
}

QString AppInfo::name() const {
  return d->name;
}

QString AppInfo::description() const {
  return d->description;
}

QString AppInfo::shortname() const {
  return d->shortname;
}

QString AppInfo::author() const {
  return d->author;
}

QString AppInfo::iconPath() const {
  return d->iconPath;
}

AppInfo::AppState AppInfo::state() const {
  return d->state;
}

qreal AppInfo::progress() const {
  return d->progress;
}

QString AppInfo::serverId() const {
  return d->serverid;
}
QString AppInfo::wgtPath() const {
  return d->wgtpath;
}
QString AppInfo::fileName() const {
  return d->filename;
}

QString AppInfo::categoryId() const {
  return d->categoryid;
}
QString AppInfo::categoryName() const {
  return d->categoryname;
}
QString AppInfo::deviceId() const {
  return d->deviceid;
}
QString AppInfo::deviceName() const {
  return d->devicename;
}
double AppInfo::createdTime() const {
  return d->createdtime;
}

void AppInfo::setState(const AppState state) {
  d->state = state;
}
void AppInfo::setState(const int state) {
  d->state = (enum AppState)state;
}

void AppInfo::setProgress(const qreal progress) {
  d->progress = progress;
}

void AppInfo::read(const QJsonObject& json) {
  d->id = json["id"].toString();
  d->version = json["version"].toString();
  d->width = json["width"].toInt();
  d->height = json["height"].toInt();
  d->name = json["name"].toString();
  d->description = json["description"].toString();
  d->shortname = json["shortname"].toString();
  d->author = json["author"].toString();
  d->iconPath = json["icon"].toString();
  QFileInfo fi(d->iconPath);
  if (!fi.isFile()) {
    d->iconPath = "";
  }

  d->state = Launch;
}

void AppInfo::readFromServer(const QJsonObject& json) {
  d->name = json["appName"].toString();
  d->description = json["appAbstract"].toString();
  d->version = json["versionName"].toString();
  d->id = json["appIdCustom"].toString() + "@" + d->version;
  d->serverid = json["appId"].toString();
  d->wgtpath = json["verFilePath"].toString();
  d->filename = json["verFilePath"].toString().section('/', -1);
  d->author = json["developerName"].toString();
  d->categoryid = json["typeId"].toString();
  d->categoryname = json["typeName"].toString();
  d->deviceid = json["appDeviceTypeId"].toString();
  d->devicename = json["appDeviceTypeName"].toString();
  d->createdtime = json["updateDate"].toDouble();
  d->iconPath = json["imagePath"].toString();

  if (json["imagePath"].toString().isEmpty()) {
    d->iconPath = "";
  } else {
    d->iconPath = getIconUrl(json["imagePath"].toString());
  }
  d->state = Install;
  d->progress = 0.0;
}
