/*
 * Copyright (C) 2016 The Qt Company Ltd.
 * Copyright (c) 2018-2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef NATIVEAPPMODE_H
#define NATIVEAPPMODE_H

#include <QtCore/QAbstractListModel>
#include "appinfo.h"

class NativeAppModel : public QAbstractListModel {
  Q_OBJECT

 public:
  enum ModelRole {
    IconRole = Qt::DisplayRole,
    NameRole = Qt::DecorationRole,
    IdRole = Qt::UserRole,
    VersionRole,
    DescriptionRole,
    ShortNameRole,
    AuthorRole
  };
  Q_ENUM(ModelRole)

  explicit NativeAppModel(QObject* parent = nullptr);
  ~NativeAppModel();

  int rowCount(const QModelIndex& parent = QModelIndex()) const override;

  QVariant data(const QModelIndex& index,
                int role = Qt::DisplayRole) const override;
  QHash<int, QByteArray> roleNames() const override;
  Q_INVOKABLE QString id(int index) const;
  Q_INVOKABLE QString name(int index) const;
  Q_INVOKABLE int launch(const QString& application);
  Q_INVOKABLE void uninstall(int index);
  Q_INVOKABLE void refresh();

  void appChanged(const QString& info);

 signals:
  void applistChanged(const QList<AppInfo>& applist);

 private:
  class Private;
  Private* d;
};

#endif  // NATIVEAPPMODE_H
