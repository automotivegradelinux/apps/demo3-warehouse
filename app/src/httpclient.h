/*
 * Copyright (c) 2018-2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef HTTPCLIENT_H
#define HTTPCLIENT_H

#include <functional>
#include <QtGlobal>

class QString;
class QByteArray;
class QNetworkRequest;
class QNetworkReply;
class QNetworkAccessManager;
class HttpClientPrivate;

class HttpClient {
 public:
  HttpClient(const QString& url);
  ~HttpClient();

  HttpClient& manager(QNetworkAccessManager* manager);

  HttpClient& debug(bool debug);

  HttpClient& param(const QString& name, const QString& value);

  HttpClient& header(const QString& header, const QString& value);

  void get(std::function<void(const QString&)> successHandler,
           std::function<void(const QString&)> errorHandler = NULL,
           const char* encoding = "UTF-8");

  void download(
      const QString& savePath,
      std::function<void(const QString&)> successHandler = NULL,
      std::function<void(const QString&)> errorHandler = NULL,
      std::function<void(const qint64, const qint64)> progressHandler = NULL);

  void download(
      std::function<void(const QByteArray&)> readyRead,
      std::function<void(const QString&)> successHandler = NULL,
      std::function<void(const QString&)> errorHandler = NULL,
      std::function<void(const qint64, const qint64)> progressHandler = NULL);

 private:
  HttpClientPrivate* d;
};

#endif  // !HTTPCLIENT_H
