/*
 * Copyright (C) 2018 The Qt Company Ltd.
 * Copyright (c) 2018-2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import QtQuick 2.6
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.0

Page {
    id: root

    property alias model: listView.model

    property int appSearchType: -1
    property string appSearchKeyword: ""
    property int appSize: 0
    property int pageSize: 0

    property int currentPageIndex: 0
    property int nPullHeight: 100

    property int oldContentY: 0

    BusyIndicator {
        id: prevBusyIndicator
        anchors.horizontalCenter: parent.horizontalCenter
        implicitWidth: 60
        implicitHeight: 60
        running: false
    }
	BusyIndicator {
        id: nextBusyIndicator
        anchors.horizontalCenter: parent.horizontalCenter
		anchors.bottom: parent.bottom
        implicitWidth: 60
        implicitHeight: 60
        running: false
    }

    StackView {
        id: stack
        initialItem: listPage
        anchors.fill: parent
        anchors.leftMargin: root.width * 0.04
        anchors.rightMargin: root.width * 0.04
        anchors.topMargin: root.width * 0.04
        Component.onCompleted: {
                listView.model.clearList();
                stack.push("qrc:/pages/SearchPage.qml",
                   {appSearchKeyword: appSearchKeyword,
                    appSearchType: appSearchType,
                    parentPage: root}, StackView.Immediate)
                console.log("depth=", depth.toString())
        }
    }
    Page {
        id: listPage
        anchors.fill: parent
        RowLayout {
            id: keywordLayout
            //height: parent.width / 3
            width: parent.width
            //anchors.top: root.top
            //anchors.topMargin: 10

            spacing: 20
            ColumnLayout {
                spacing: 5
                RowLayout {
                Label {
                    Layout.fillWidth: true
                    text: qsTr("KeyWord")
                    font.pixelSize: 32
                    font.bold: true
                    color: '#00ADDC'
                }
                Item {
                    id: backItem
                    width: 80
                    height: 40
                    Layout.alignment: Qt.AlignRight | Qt.AlignTop
                    Image {
                        //Layout.alignment: Qt.AlignRight | At.AlignVCenter
                        id: backBtn
                        anchors.fill: parent
                        source: 'qrc:/images/Back.svg'
                    }
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            listView.model.clearList();
                            stack.push("qrc:/pages/SearchPage.qml",
                               {appSearchKeyword: appSearchKeyword,
                                appSearchType: appSearchType,
                                parentPage: root}, StackView.Immediate)
                        }
                    }
                }
                }
                RowLayout {
                    width: root.width
                    spacing: 10
                    TextEdit {
                        id: textInputKeyWord
                        Layout.fillWidth: true
                        text: appSearchKeyword == "" ? qsTr("Please input keyword!") : appSearchKeyword
                        font.pixelSize: 32
                        font.bold: true
                        font.wordSpacing: -1
                        font.letterSpacing: 0
                        clip: true
                        font.weight: Font.Normal
                        font.capitalization: Font.MixedCase
                        onFocusChanged: {
                            if(focus === false){
                                if(text === ""){
                                    text = qsTr("Please input keyword!")
                                }
                            }
                        }
                        Rectangle {
                            anchors.fill: parent
                            color:"transparent"
                            border.color: "#66FF99"
                            border.width: 2
                            z: -1
                        }
                    }
                    SButton {
                        text: 'X'
                        anchors.rightMargin: 100

                        visible: textInputKeyWord.text != ""
                        onClicked: {
                            textInputKeyWord.text = "";
                            textInputKeyWord.forceActiveFocus();
                        }
                        implicitWidth: 40
                        implicitHeight: 40
                    }
                    Item {
                        id: searchBtn
                        width: 140
                        height: 40
                        Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                        SButton {
                            anchors.fill:parent
                            enabled: (textInputKeyWord.text != ""
                                      && textInputKeyWord.text != "Please input keyword!")
                            text: 'Search'
                            onClicked: {
                                listView.model.clearList();
                                if(textInputKeyWord.text != "Please input keyword!") {
                                    appSearchKeyword = textInputKeyWord.text;
                                } else {
                                    appSearchKeyword = "";
                                }
                                popBack(appSearchKeyword, appSearchType);
                            }
                        }
                    }
                }
            }
        }

        ListView {
            id: listView
            width: parent.width
            height: parent.height - keywordLayout.height
            anchors.top: keywordLayout.bottom

            clip: true

            delegate: MouseArea {
                id: delegate
                width: listView.width
                height: width / 6

                RowLayout {
                    anchors.fill: parent

                    Item {
                        Layout.preferredWidth: 80
                        Layout.preferredHeight: 80

                        MouseArea{
                            anchors.fill: parent
                            z: 2
                            onClicked: {
                                stack.push("qrc:/pages/DetailPage.qml", {stack: stack, model: model}, StackView.Immediate)
                            }
                        }

                        DImage {
                            id: imageicon
                            icon: model.icon
                            name: model.name
                        }
                    }

                    ColumnLayout {
                        Label {
                            Layout.fillWidth: true
                            text: model.name.toUpperCase()
                            color: '#00ADDC'
                        }
                        Label {
                            text: 'Version: ' + model.version
                            font.pixelSize: 16
                            font.italic: true
                        }
                        Label {
                            text: 'Description: ' + model.description
                            font.pixelSize: 16
                            wrapMode: Text.Wrap
                            elide: Text.ElideRight
                            Layout.preferredWidth: 400
                            Layout.preferredHeight: 40
                        }
                    }
                    ColumnLayout {
                        spacing: 5
                        Layout.alignment: Qt.AlignRight | Qt.AlignVCenter

                        Button {
                            anchors.right: parent.right
                            anchors.rightMargin: 6
                            text: model.statetext
                            onClicked: {
                                if (model.statetext === 'Install' || model.statetext === 'Update') {
                                    listView.model.install(model.index)
                                } else if (model.statetext === 'Launch') {
                                    if (listView.model.launch(model.id) > 1) {
                                        homescreenHandler.tapShortcut(model.name)
                                    } else {
                                        console.warn('app cannot be launched')
                                    }
                                }
                            }
                            implicitWidth: 140
                            implicitHeight: 40
                        }
                    }
                }

                DownloadBar {
                    anchors.fill: parent
                    progress: model.progress
                }
                Image {
                    source: 'qrc:/images/DividingLine.svg'
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.top: parent.top
                    visible: model.index > 0
                }
            }

            onMovementStarted: {
                oldContentY = contentY
            }

            onFlickStarted: {
                if (contentY < -nPullHeight && prevBusyIndicator.running == false) {
                    prevBusyIndicator.running = true
                    listView.model.getPrevPage(currentPageIndex)
                } else if (nextBusyIndicator.running == false) {
                    if ((contentHeight > 0) && (contentY > (contentHeight - height + nPullHeight))) {
                        nextBusyIndicator.running = true
                        listView.model.getNextPage(currentPageIndex)
                    }
                }
            }
        }

        Connections {
            target: listView.model
            onRequestCompleted: {
                if (isPrev) {
                    if (pageIndex > 0 && offsetSize == 0) {
                        listView.model.getPrevPage(pageIndex - 1)
                        return
                    }
                    currentPageIndex = pageIndex
                    prevBusyIndicator.running = false
                } else {
                    nextBusyIndicator.running = false

                    if (listView.contentHeight > listView.height){
                        var itemHeight = listView.contentHeight / listView.count

                        if (offsetSize <= 0) {
                            listView.contentY = listView.contentHeight - listView.height
                        } else if (offsetSize < pageSize){
                            var viewCount = listView.height / itemHeight

                            if (offsetSize <= (viewCount / 2)) {
                                listView.contentY = oldContentY + (offsetSize * itemHeight)
                            } else {
                                listView.contentY = oldContentY + (listView.height / 2)
                            }
                        } else {
                            currentPageIndex = pageIndex
                        }
                    }
                }
            }
        }
    }

    function popBack(name, typeid)
    {
        appSearchKeyword = name;
        appSearchType = typeid;

        currentPageIndex = 0;
        listView.model.getPrevPage(currentPageIndex, appSearchKeyword, appSearchType);
        stack.pop(StackView.Immediate);
        console.log("SearchList:Keyword=", appSearchKeyword, "typeid=", appSearchType.toString());
    }
}
