/*
 * Copyright (C) 2018 The Qt Company Ltd.
 * Copyright (c) 2018-2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import QtQuick 2.6
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.0

 Page {
    id: detailpage
    visible: true

    property StackView stack: null
    property var model

    Item {
        id: backItem
        width: 80
        height: 40

        Image {
            anchors.fill: parent
            source: 'qrc:/images/Back.svg'
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                stack.pop(StackView.Immediate)
            }
        }
    }

    RowLayout {
        id: infoLayout

        height: parent.width / 3
        width: parent.width
        anchors.top: backItem.bottom
        anchors.topMargin: 10

        spacing: 20

        Item {
            Layout.preferredWidth: 200
            Layout.preferredHeight: 200

            DImage {
                id: imageicon
                icon: model.icon
                name: model.name
            }
        }

        ColumnLayout {
            spacing: 5
            Label {
                Layout.fillWidth: true
                text: model.name.toUpperCase()
                font.pixelSize: 32
                color: '#00ADDC'
            }
            Label {
                text: 'Author: ' + model.author
                font.pixelSize: 24
            }
            Label {
                text: 'Version: ' + model.version
                font.pixelSize: 16
                font.italic: true
            }
            Label {
                text: 'Category: ' + model.category
                font.pixelSize: 16
            }
            Label {
                text: 'UpdateTime: ' + new Date(model.createdtime)
                font.pixelSize: 16
            }
        }
    }

    Image {
        id: dividingLine
        width: parent.width
        anchors.top: infoLayout.bottom
        anchors.topMargin: 10
        anchors.horizontalCenter: parent.horizontalCenter
        source: 'qrc:/images/DividingLine.svg'
    }

    Label {
        width: parent.width
        anchors.top: dividingLine.bottom
        anchors.topMargin: 20

        text: model.description
        wrapMode: Text.Wrap
        font.pixelSize: 40
    }
 }
