/*
 * Copyright (C) 2018 The Qt Company Ltd.
 * Copyright (c) 2018-2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import QtQuick 2.6
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.1

Page {
    id: root

    property variant parentPage: null
    property int appSearchType: -1
    property string appSearchKeyword: ""
    property variant appTypeSelectlist:[
            "Automotive",
            "Operation System",
            "Connectivity",
            "Graphics",
            "Navigation",
            "Multimedia",
            "Nativate APP",
            "AGL APP",
            "Web APP",
            "Other"]

    RowLayout {
        id: keywordLayout
        //height: parent.width / 3
        width: parent.width
        //anchors.top: root.top
        //anchors.topMargin: 10

        spacing: 20
        ColumnLayout {
            spacing: 5
            Label {
                Layout.fillWidth: true
                text: qsTr("KeyWord:")
                font.pixelSize: 32
                font.bold: true
                color: '#00ADDC'
            }
            RowLayout {
                width: root.width
                spacing: 10
                TextEdit {
                    id: textInputKeyWord
                    Layout.fillWidth: true
                    text: appSearchKeyword == "" ? qsTr("Please input keyword!") : appSearchKeyword
                    font.pixelSize: 32
                    font.bold: true
                    font.wordSpacing: -1
                    font.letterSpacing: 0
                    clip: true
                    font.weight: Font.Normal
                    font.capitalization: Font.MixedCase
                    onFocusChanged: {
                        if(focus === false){
                            if(text === ""){
                                text = qsTr("Please input keyword!")
                            }
                        }
                    }
                    Rectangle {
                        anchors.fill: parent
                        color:"transparent"
                        border.color: "#66FF99"
                        border.width: 2
                        z: -1
                    }
                }
                SButton {
                    text: 'X'
                    anchors.rightMargin: 100

                    visible: textInputKeyWord.text != ""
                    onClicked: {
                        textInputKeyWord.text = "";
                        textInputKeyWord.forceActiveFocus();
                    }


                    implicitWidth: 40
                    implicitHeight: 40
                }
                Item {
                    id: searchBtn
                    width: 140
                    height: 40
                    Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                    SButton {
                        anchors.fill:parent
                        enabled: (textInputKeyWord.text != ""
                                  && textInputKeyWord.text != "Please input keyword!")
                                || appSearchType != -1
                        text: 'Search'
                        onClicked: {
                            getSearchList();
                        }
                    }
                }
            }
            Label {
                Layout.fillWidth: true
                text: qsTr("App Type:")
                font.pixelSize: 32
                font.bold: true
                color: '#00ADDC'
            }
        }
    }

    RowLayout {
        id: infoLayout
        height: root.width / 3 * 2
        width: root.width
        anchors.top: keywordLayout.bottom
        anchors.topMargin: 10
        spacing: 20
        ColumnLayout {
            Layout.alignment: Qt.AlignCenter | Qt.AlignVCenter
            spacing: 60

            SButton {
                text: appTypeSelectlist[0]
                highlighted: appSearchType == getSelectAppTypeId(text)
                onClicked:{
                    var appTypeid = getSelectAppTypeId(text);
                    if(appSearchType === appTypeid) {
                        appSearchType = -1;
                    } else {
                        appSearchType = appTypeid;
                    }
                    console.log("appSearchType=", appSearchType.toString(), appTypeid.toString());
                }
                implicitWidth: 320
                implicitHeight: 60
            }
            SButton {
                text: appTypeSelectlist[2]
                highlighted: appSearchType == getSelectAppTypeId(text)
                onClicked:{
                    var appTypeid = getSelectAppTypeId(text);
                    if(appSearchType == appTypeid) {
                        appSearchType = -1;
                    } else {
                        appSearchType = appTypeid;
                    }
                    console.log("appSearchType=", appSearchType.toString(), appSearchType.toString());
                }
                implicitWidth: 320
                implicitHeight: 60
            }
            SButton {
                text: appTypeSelectlist[4]//qsTr("Navigation")
                highlighted: appSearchType == getSelectAppTypeId(text)
                onClicked:{
                    var appTypeid = getSelectAppTypeId(text);
                    if(appSearchType == appTypeid) {
                        appSearchType = -1;
                    } else {
                        appSearchType = appTypeid;
                    }
                    console.log("appSearchType=", appSearchType.toString(), appSearchType.toString());
                }
                implicitWidth: 320
                implicitHeight: 60
            }
            SButton {
                text: appTypeSelectlist[6]//qsTr("Nativate APP")
                highlighted: appSearchType == getSelectAppTypeId(text)
                onClicked:{
                    var appTypeid = getSelectAppTypeId(text);
                    if(appSearchType == appTypeid) {
                        appSearchType = -1;
                    } else {
                        appSearchType = appTypeid;
                    }
                }
                implicitWidth: 320
                implicitHeight: 60
            }
            SButton {
                text: appTypeSelectlist[8]//qsTr("Web APP")
                highlighted: appSearchType == getSelectAppTypeId(text)
                onClicked:{
                    var appTypeid = getSelectAppTypeId(text);
                    if(appSearchType == appTypeid) {
                        appSearchType = -1;
                    } else {
                        appSearchType = appTypeid;
                    }
                }
                implicitWidth: 320
                implicitHeight: 60
            }
        }
        ColumnLayout {
            Layout.alignment: Qt.AlignCenter | Qt.AlignVCenter
            spacing: 60

            SButton {
                text: appTypeSelectlist[1]//qsTr("Operation System")
                highlighted: appSearchType == getSelectAppTypeId(text)
                onClicked:{
                    var appTypeid = getSelectAppTypeId(text);
                    if(appSearchType == appTypeid) {
                        appSearchType = -1;
                    } else {
                        appSearchType = appTypeid;
                    }
                }
                implicitWidth: 320
                implicitHeight: 60
            }
            SButton {
                text: appTypeSelectlist[3]//qsTr("Graphics")
                highlighted: appSearchType == getSelectAppTypeId(text)
                onClicked:{
                    var appTypeid = getSelectAppTypeId(text);
                    if(appSearchType == appTypeid) {
                        appSearchType = -1;
                    } else {
                        appSearchType = appTypeid;
                    }
                }
                implicitWidth: 320
                implicitHeight: 60
            }
            SButton {
                text: appTypeSelectlist[5]//qsTr("Multimedia")
                highlighted: appSearchType === getSelectAppTypeId(text)
                onClicked:{
                    var appTypeid = getSelectAppTypeId(text);
                    if(appSearchType == appTypeid) {
                        appSearchType = -1;
                    } else {
                        appSearchType = appTypeid;
                    }
                }
                implicitWidth: 320
                implicitHeight: 60
            }
            SButton {
                text: appTypeSelectlist[7]//qsTr("AGL APP")
                highlighted: appSearchType === getSelectAppTypeId(text)
                onClicked:{
                    var appTypeid = getSelectAppTypeId(text);
                    if(appSearchType == appTypeid) {
                        appSearchType = -1;
                    } else {
                        appSearchType = appTypeid;
                    }
                }
                implicitWidth: 320
                implicitHeight: 60
            }
            SButton {
                text: appTypeSelectlist[9]//qsTr("Other")
                highlighted: appSearchType === getSelectAppTypeId(text)
                onClicked:{
                    var appTypeid = getSelectAppTypeId(text);
                    if(appSearchType === appTypeid) {
                        appSearchType = -1;
                    } else {
                        appSearchType = appTypeid;
                    }
                }
                implicitWidth: 320
                implicitHeight: 60
            }
        }


    }


    Rectangle {
        anchors.fill: infoLayout
        color: 'transparent'
        border.color: 'grey'
        z: -1
    }


    function getSelectAppTypeId(name){
        for(var key in appTypeSelectlist){
            if(name == appTypeSelectlist[key]){
                return Number(key);
            }
        }
        return -1;
    }

    function getSearchList()
    {
        if(textInputKeyWord.text != "Please input keyword!") {
            appSearchKeyword = textInputKeyWord.text;
        } else {
            appSearchKeyword = "";
        }

        console.log("SearchPage:KeyWord=", appSearchKeyword,
                    "appTypeid=", appSearchType.toString());
        parentPage.popBack(appSearchKeyword, appSearchType);
    }
}
