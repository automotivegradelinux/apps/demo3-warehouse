/*
 * Copyright (C) 2018 The Qt Company Ltd.
 * Copyright (c) 2018-2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import QtQuick 2.6
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.0

Page {
    id: root
    property alias model: listView.model

    property int nPullHeight: 300

    BusyIndicator {
        id: busyIndicator
        anchors.horizontalCenter: parent.horizontalCenter
        implicitWidth: 60
        implicitHeight: 60
        running: false
    }

    ListView {
        id: listView
        anchors.fill: parent
        anchors.margins: root.width * 0.075
        clip: true

        delegate: MouseArea {
            id: delegate
            width: listView.width
            height: width / 6
            RowLayout {
                anchors.fill: parent
                Item {
                    Layout.preferredWidth: 100
                    Layout.preferredHeight: 100

                    DImage {
                        id: imageicon
                        icon: model.icon != ""?'file://' + model.icon : ''
                        name: model.name
                    }
                }
                ColumnLayout {
                    Label {
                        Layout.fillWidth: true
                        text: model.name.toUpperCase()
                        color: '#00ADDC'
                    }
                    Label {
                        text: 'Version: ' + model.version
                        font.pixelSize: 16
                        font.italic: true
                    }
                    Label {
                        text: 'Description: ' + model.description
                        font.pixelSize: 16
                        wrapMode: Text.Wrap
                        elide: Text.ElideRight
                        Layout.preferredWidth: 400
                        Layout.preferredHeight: 40
                    }
                }
                ColumnLayout {
                    spacing: 5
                    Layout.alignment: Qt.AlignRight | Qt.AlignVCenter

                    Button {
						anchors.right: parent.right
						anchors.rightMargin: 6
                        text: 'Launch'
                        onClicked: {
                            if (listView.model.launch(model.id) > 1) {
                                homescreenHandler.tapShortcut(model.name)
                            } else {
                                console.warn('app cannot be launched')
                            }
                        }
                        implicitWidth: 140
                        implicitHeight: 40
                    }
                    Button {
						anchors.right: parent.right
						anchors.rightMargin: 6
						visible: model.name.toUpperCase() != "HOMESCREEN" && model.name.toUpperCase() != "LAUNCHER"
                        text: 'Uninstall'
                        onClicked: {
                            listView.model.uninstall(model.index)
                        }
                        implicitWidth: 140
                        implicitHeight: 40
                    }
                }
            }
            Image {
                source: 'qrc:/images/DividingLine.svg'
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.top: parent.top
                visible: model.index > 0
            }

        }

        states: [
            State {
                name: "refreshState"; when: listView.contentY < -nPullHeight
                StateChangeScript {
                    name: "refreshScript"
                    script: refreshModel()
                }
            }
        ]
    }

    function refreshModel() {
        listView.y = nPullHeight;

        busyIndicator.running = true
        refreshTimer.start();
    }

    Timer {
        id: refreshTimer
        interval: 1000
        repeat: false
        running: false
        onTriggered: {
            listView.model.refresh()
            refreshAnimation.start();
        }
    }

    NumberAnimation {
        id: refreshAnimation
        target: listView
        property: "y"
        duration: 100
        from: nPullHeight
        to: 0
        onStopped: {
            busyIndicator.running = false
            listView.y = 0;
        }
    }

}

