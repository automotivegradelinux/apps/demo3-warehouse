/*
 * Copyright (C) 2018 The Qt Company Ltd.
 * Copyright (c) 2018-2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import QtQuick 2.6
import QtQuick.Controls 2.0

ProgressBar {
    property real progress: 0

    id: progressbar
    z: -1
    value: (progress/100)

    background: Rectangle {
        anchors.fill: parent
        color: "transparent"
        opacity: 0
    }

    contentItem: Item {
        anchors.fill: parent

		Rectangle {
			width: progressbar.value * parent.width
			height: parent.height
            opacity: 0.3
			color: "#32d0eb"
		}
	}
}