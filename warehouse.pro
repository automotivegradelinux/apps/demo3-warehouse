TEMPLATE = subdirs

load(configure)

SUBDIRS = interfaces app package
app.depends = interfaces
package.depends += app
